package com.example.mathewholden.mholden_final;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.order;


public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 6;

	// Database Name
	private static final String DATABASE_NAME = "budgetManager";

	// Contacts table name
	private static final String TABLE_BUDGET = "budget";

	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_BUDGET = "budget";
	private static final String KEY_HOUSE = "house";
	private static final String KEY_GROCERIES = "groceries";
	private static final String KEY_GAS = "gas";
	private static final String KEY_ELECTRIC = "electric";
	private static final String KEY_CABLE = "cable";
	private static final String KEY_CAR = "car";
	private static final String KEY_DR = "dr";
	private static final String KEY_CLOTHING = "clothing";
	private static final String KEY_ENTERTAINMENT = "entertainment";
	private static final String KEY_ITEMS = "items";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_BUDGET + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_BUDGET + " DOUBLE,"
				+ KEY_HOUSE + " DOUBLE," + KEY_GROCERIES + " DOUBLE,"
				+ KEY_GAS + " DOUBLE," +KEY_ELECTRIC + " DOUBLE,"
				+ KEY_CABLE + " DOUBLE," +KEY_CAR + " DOUBLE," +KEY_DR + " DOUBLE,"
				+KEY_CLOTHING + " DOUBLE," +KEY_ENTERTAINMENT + " DOUBLE," +KEY_ITEMS + " DOUBLE" + ")";
		/**
		 *  KEY PRICE INTEGER OR STRING?
		 */
		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUDGET);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new Order to SQL DB
	void addOrder(Budget budget) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_BUDGET, budget.getBudget());
		values.put(KEY_HOUSE, budget.getHouse());
		values.put(KEY_GROCERIES, budget.getGroceries());
		values.put(KEY_CABLE, budget.getCable());
		values.put(KEY_CAR, budget.getCar());
		values.put(KEY_CLOTHING, budget.getClothes());
		values.put(KEY_ELECTRIC, budget.getElectric());
		values.put(KEY_ENTERTAINMENT, budget.getEntertain());
		values.put(KEY_DR, budget.getDr());
		values.put(KEY_ITEMS, budget.getHouseItems());
		values.put(KEY_GAS, budget.getGas());

		// Inserting Row
		db.insert(TABLE_BUDGET, null, values);
		db.close(); // Closing database connection
	}


	// Getting single order
	Budget getBudgetByID(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT  * FROM " + TABLE_BUDGET + " WHERE " + KEY_ID + " = " + "'" + id + "'";

		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor != null)
			cursor.moveToFirst();

		Budget budget = new Budget(cursor.getInt(0),
				cursor.getFloat(1),
				cursor.getFloat(2),
				cursor.getFloat(3),
				cursor.getFloat(4),
				cursor.getFloat(5),
				cursor.getFloat(6),
				cursor.getFloat(7),
				cursor.getFloat(8),
				cursor.getFloat(9),
				cursor.getFloat(10),
				cursor.getFloat(11));


		// return order
		return budget;
	}


//	// Getting All Orders greater than price
//	public List<Budget> getOrderByPrice(float price) {
//		List<Budget> orderList = new ArrayList<Budget>();
//		// Select All Query
//		String selectQuery = "SELECT  * FROM " + TABLE_BUDGET + " WHERE " + KEY_PRICE + " >= " + "'" + price + "'";
//
//        SQLiteDatabase db = this.getReadableDatabase();
//		Cursor cursor = db.rawQuery(selectQuery, null);
//
//		// looping through all rows and adding to list
//		if (cursor.moveToFirst()) {
//			do {
//                Order order = new Order();
//                order.setId(Integer.parseInt(cursor.getString(0)));
//                order.setFirstName(cursor.getString(1));
//                order.setLastName(cursor.getString(2));
//                order.setChocolateType(cursor.getString(3));
//                order.setNumOfBarsPurchased(cursor.getInt(4));
//
//                int shippingType = cursor.getInt(5);
//                if (shippingType == 1) {
//                    order.setShippingType(true);
//                } else {
//                    order.setShippingType(false);
//                }
//                order.setPrice(cursor.getFloat(6));
//
//
//                // Adding order to list
//                orderList.add(order);
//			} while (cursor.moveToNext());
//		}
//
//		// return order list
//		return orderList;
//	}

	// Getting All Orders
	public List<Budget> getAllBudgets() {
		List<Budget> budgetList = new ArrayList<>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_BUDGET;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Budget budget = new Budget();
				budget.setId(Integer.parseInt(cursor.getString(0)));
				budget.setBudget(cursor.getFloat(1));
				budget.setCar(cursor.getFloat(2));
				budget.setCable(cursor.getFloat(3));
				budget.setClothes(cursor.getFloat(4));
				budget.setDr(cursor.getFloat(5));
				budget.setEntertain(cursor.getFloat(6));
				budget.setElectric(cursor.getFloat(7));
				budget.setGas(cursor.getFloat(8));
				budget.setGroceries(cursor.getFloat(9));
				budget.setHouseItems(cursor.getFloat(10));
				budget.setHouse(cursor.getFloat(11));

				// Adding order to list
				budgetList.add(budget);
			} while (cursor.moveToNext());
		}

		// return order list
		return budgetList;
	}


	// Getting Orders Count
	public int getOrdersCount() {
		String countQuery = "SELECT  * FROM " + TABLE_BUDGET;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int county = cursor.getCount();
		cursor.close();

		// return count
		return county;
	}
}
