package com.example.mathewholden.mholden_final;

import android.os.Parcel;
import android.os.Parcelable;

import static android.R.attr.value;

/**
 * Created by Mathew Holden on 9/15/2017.
 */

public class Budget implements Parcelable {
    public Float budget;
    public Float house;
    public Float groceries;
    public Float gas;
    public Float electric;
    public Float cable;
    public Float car;
    public Float dr;
    public Float clothes;
    public Float entertain;
    public Float houseItems;
    public int id;

    public Budget(Float budget, int i) {
    }

    public Float getRemain() {
        float remain = 0;
        remain = getBudget() - getBill();
        return remain;
    }

    public void setRemain(Float remain) {
        this.remain = remain;
    }

    public Float remain;

    public Float getBill() {
        float bill = 0;
        bill = house + groceries + electric+ cable+car+ dr+clothes+ entertain+ houseItems + gas;
        return bill;
    }

    public void setBill(Float bill) {
        this.bill = bill;
    }

    public  Float bill;

    public Float BillMath( Float house, Float groceries, Float gas, Float electric, Float cable,Float car, Float dr, Float clothes, Float entertain, Float houseItems){
      float bill = 0;
        bill = house + groceries + electric+ cable+car+ dr+clothes+ entertain+ houseItems + gas;
        return bill;
    }
    public Budget(int id, Float budget, Float house, Float groceries, Float gas, Float electric, Float cable,Float car, Float dr, Float clothes, Float entertain, Float houseItems) {
        this.id = id;
        this.budget = budget;
        this.house = house;
        this.groceries = groceries;
        this.gas = gas;
        this.electric = electric;
        this.cable = cable;
        this.car = car;
        this.dr = dr;
        this.clothes = clothes;
        this.entertain = entertain;
        this.houseItems = houseItems;
    }

    public Budget() {

    }


    public void budget(Float budget, Float house, Float groceries, Float gas, Float electric, Float cable,Float car, Float dr, Float clothes, Float entertain, Float houseItems )
    {

        this.budget = budget;
        this.house = house;
        this.groceries = groceries;
        this.gas = gas;
        this.electric = electric;
        this.cable = cable;
        this.car = car;
        this.dr = dr;
        this.clothes = clothes;
        this.entertain = entertain;
        this.houseItems = houseItems;

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public Float getBudget() {
        return budget;
    }

    public void setBudget(Float budget) {
        this.budget = budget;
    }

    public Float getHouse() {
        return house;
    }

    public void setHouse(Float house) {
        this.house = house;
    }

    public Float getGroceries() {
        return groceries;
    }

    public void setGroceries(Float groceries) {
        this.groceries = groceries;
    }

    public Float getGas() {
        return gas;
    }

    public void setGas(Float gas) {
        this.gas = gas;
    }

    public Float getElectric() {
        return electric;
    }

    public void setElectric(Float electric) {
        this.electric = electric;
    }

    public Float getCable() {
        return cable;
    }

    public void setCable(Float cable) {
        this.cable = cable;
    }

    public Float getCar() {
        return car;
    }

    public void setCar(Float car) {
        this.car = car;
    }

    public Float getDr() {
        return dr;
    }

    public void setDr(Float dr) {
        this.dr = dr;
    }

    public Float getClothes() {
        return clothes;
    }

    public void setClothes(Float clothes) {
        this.clothes = clothes;
    }

    public Float getEntertain() {
        return entertain;
    }

    public void setEntertain(Float entertain) {
        this.entertain = entertain;
    }

    public Float getHouseItems() {
        return houseItems;
    }

    public void setHouseItems(Float houseItems) {
        this.houseItems = houseItems;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeFloat(this.budget      );
        parcel.writeFloat(this.cable       );
        parcel.writeFloat(this.car         );
        parcel.writeFloat(this.clothes     );
        parcel.writeFloat(this.dr          );
        parcel.writeFloat(this.electric    );
        parcel.writeFloat(this.entertain   );
        parcel.writeFloat(this.gas         );
        parcel.writeFloat(this.groceries   );
        parcel.writeFloat(this.house       );
        parcel.writeFloat(this.houseItems  );
        
 }
 protected Budget(Parcel in){
     this.id = in.readInt();
     this.budget = in.readFloat();
     this.cable = in.readFloat();
     this.car = in.readFloat();
     this.clothes = in.readFloat();
     this.dr = in.readFloat();
     this.electric = in.readFloat();
     this.entertain = in.readFloat();
     this.gas = in.readFloat();
     this.groceries = in.readFloat();
     this.house = in.readFloat();
     this.houseItems = in.readFloat();
 }
    public static final Creator<Budget> CREATOR = new Creator<Budget>() {
        @Override
        public Budget createFromParcel(Parcel source) {
            return new Budget(source);
        }

        @Override
        public Budget[] newArray(int size) {
            return new Budget[size];
        }
    };
}
