package com.example.mathewholden.mholden_final;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;

import static com.example.mathewholden.mholden_final.R.id.btnPie;
import static com.example.mathewholden.mholden_final.R.id.fab;
import static com.example.mathewholden.mholden_final.R.id.txtBudget;
import static com.example.mathewholden.mholden_final.R.id.txtId;
//import static com.example.mathewholden.mholden_final.R.id.txtResults;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 10;

    private EditText txtBudget;
    private EditText txtHouse;
    private EditText txtGroceries;
    private EditText txtGas;
    private EditText txtElectric;
    private EditText txtCable;
    private EditText txtCar;
    private EditText txtDr;
    private EditText txtClothing;
    private EditText txtEntertainment;
    private EditText txtHouseItems;
    private Button btnSave;
    private TextView txtResults;
    private EditText txtId;
   // private Button btnLi;
private Button btnGet;
    DatabaseHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

db = new DatabaseHandler(this);
        txtBudget = (EditText)findViewById(R.id.txtBudget);
        txtHouse = (EditText)findViewById(R.id.txtHouse);
        txtGroceries = (EditText)findViewById(R.id.txtGroceries);
        txtGas = (EditText)findViewById(R.id.txtGas);
        txtElectric = (EditText)findViewById(R.id.txtElectric);
        txtEntertainment = (EditText)findViewById(R.id.txtEntertain);
        txtCable = (EditText)findViewById(R.id.txtCable);
        txtCar = (EditText)findViewById(R.id.txtCar);
        txtClothing = (EditText)findViewById(R.id.txtClothing);
        txtDr = (EditText)findViewById(R.id.txtDr);
        txtHouseItems = (EditText)findViewById(R.id.txtItems);
    //    txtResults = (TextView)findViewById(R.id.txtResults);
        txtId = (EditText)findViewById(R.id.txtId);
     //   btnLi = (Button)findViewById(R.id.btnLi);
btnGet = (Button)findViewById(R.id.btnGet);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        });


btnGet.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Budget bud = db.getBudgetByID(Integer.valueOf(txtId.getText().toString()));
        txtBudget.setText(bud.getBudget().toString());
        txtHouse.setText(bud.getHouse().toString());
        txtGroceries.setText(bud.getGroceries().toString());
        txtGas.setText(bud.getGas().toString());
        txtElectric.setText(bud.getElectric().toString());
        txtEntertainment.setText(bud.getEntertain().toString());
        txtCable.setText(bud.getCable().toString());
        txtCar.setText(bud.getCar().toString());
        txtClothing.setText(bud.getClothes().toString());
        txtDr.setText(bud.getDr().toString());
        txtHouseItems.setText(bud.getHouseItems().toString());

    }
});
        // dO INTENT CALLS HERE
        // btnGoToPie.setOnClickListener(new View.OnClickListener()...
          // call goToPie function here
    }
public void SwitchAct(View m){
    Intent i = new Intent(MainActivity.this, ResultsActivity.class);
    startActivity(i);
}

    public void goToResults(View view){
       Float budget = Float.valueOf(txtBudget.getText().toString());
       Float house = Float.valueOf(txtHouse.getText().toString());
       Float groceries =Float.valueOf(txtGroceries.getText().toString());
       Float gas =Float.valueOf(txtGas.getText().toString());
       Float electric =Float.valueOf(txtElectric.getText().toString());
       Float entertainment=Float.valueOf(txtEntertainment.getText().toString());
       Float cable=Float.valueOf(txtCable.getText().toString());
       Float car=Float.valueOf(txtCar.getText().toString());
       Float dr =Float.valueOf(txtDr.getText().toString());
       Float items = Float.valueOf(txtHouseItems.getText().toString());
       Float clothes = Float.valueOf(txtClothing.getText().toString());
       Float result = house + groceries + electric+ cable+car+ dr+ clothes+ entertainment+ items + gas;
       Float remain = budget - result;
        Budget newBudget = new Budget();

        newBudget.setBudget(budget);
        newBudget.setHouse(house);
        newBudget.setGroceries(groceries);
        newBudget.setGas(gas);
        newBudget.setElectric(electric);
        newBudget.setEntertain(entertainment);
        newBudget.setCable(cable);
        newBudget.setCar(car);
        newBudget.setClothes(clothes);
        newBudget.setDr(dr);
        newBudget.setHouseItems(items);
        newBudget.setBill(result);
        newBudget.setRemain(remain);

        Intent i = new Intent(this, com.example.mathewholden.mholden_final.ResultsActivity.class);
        i.putExtra("yourKey", newBudget);
       startActivityForResult(i, REQUEST_CODE);

    }
public void goToPie(View v){
    Float budget = Float.valueOf(txtBudget.getText().toString());
    Float house = Float.valueOf(txtHouse.getText().toString());
    Float groceries =Float.valueOf(txtGroceries.getText().toString());
    Float gas =Float.valueOf(txtGas.getText().toString());
    Float electric =Float.valueOf(txtElectric.getText().toString());
    Float entertainment=Float.valueOf(txtEntertainment.getText().toString());
    Float cable=Float.valueOf(txtCable.getText().toString());
    Float car=Float.valueOf(txtCar.getText().toString());
    Float dr =Float.valueOf(txtDr.getText().toString());
    Float items = Float.valueOf(txtHouseItems.getText().toString());
    Float clothes = Float.valueOf(txtClothing.getText().toString());
    Float result = house + groceries + electric+ cable+car+ dr+ clothes+ entertainment+ items + gas;
    Float remain = budget - result;
    Budget pieBudget = new Budget();

    pieBudget.setBudget(budget);
    pieBudget.setHouse(house);
    pieBudget.setGroceries(groceries);
    pieBudget.setGas(gas);
    pieBudget.setElectric(electric);
    pieBudget.setEntertain(entertainment);
    pieBudget.setCable(cable);
    pieBudget.setCar(car);
    pieBudget.setClothes(clothes);
    pieBudget.setDr(dr);
    pieBudget.setHouseItems(items);
    pieBudget.setBill(result);
    pieBudget.setRemain(remain);

    Intent i = new Intent(this, com.example.mathewholden.mholden_final.PieResults.class);
    i.putExtra("pieKey", pieBudget);
     startActivityForResult(i, REQUEST_CODE);
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.btnNew){
            txtBudget.setText("");
            txtClothing.setText("");
            txtElectric.setText("");
            txtCar.setText("");
            txtCable.setText("");
            txtDr.setText("");
            txtElectric.setText("");
            txtGas.setText("");
            txtGroceries.setText("");
            txtHouse.setText("");
            txtHouseItems.setText("");
            txtEntertainment.setText("");

        }

        return super.onOptionsItemSelected(item);
    }


}
