package com.example.mathewholden.mholden_final;

import android.content.Intent;
import android.graphics.Color;
import android.icu.util.ULocale;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.R.attr.y;

public class PieResults extends AppCompatActivity {
    private Float[] yData;
    private String[] xData;
    PieChart piechart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        Budget newBudget = extras.getParcelable("pieKey");

       setContentView(R.layout.activity_pie_results);
      //  DatabaseHandler db;
      //  db = new DatabaseHandler(this);
        Float house = newBudget.getHouse();
        Float groceries= newBudget.getGroceries();
        Float gas = newBudget.getGas();
        Float electric =newBudget.getElectric();
        Float entertain =newBudget.getEntertain();
        Float cable =newBudget.getCable();
        Float car=newBudget.getCar();
        Float clothes=newBudget.getClothes();
        Float dr=newBudget.getDr();
        Float items=newBudget.getHouseItems();
       // Float remain =newBudget.getRemain();

        final Float[] yData={house, groceries, gas, electric,entertain,cable,car,clothes,dr,items};
        final String[] xData={"House","Groceries","Gas","Electric","Entertainment","Cable","Car",
                "Clothes","Dr","House Items"};

        piechart = (PieChart)findViewById(R.id.piechart);
        piechart.setUsePercentValues(true);

piechart.setRotationEnabled(true);
        piechart.setTransparentCircleAlpha(0);

        piechart.setCenterText("Bills");
        piechart.setCenterTextSize(25);
        piechart.setDrawEntryLabels(true);
//        Legend legend = piechart.getLegend();
//       legend.setForm(Legend.LegendForm.SQUARE);
//        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
//legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
//        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        legend.setDrawInside(true);
//        legend.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
//        legend.setXEntrySpace(7f);
//        legend.setYEntrySpace(0f);
//        legend.setYOffset(0f);
    //addDataSet();

        final ArrayList<PieEntry>yEntry = new ArrayList<>();
        final ArrayList<String>xEntry = new ArrayList<>();
        for(int i = 0; i < yData.length; i++){
            yEntry.add(new PieEntry(yData[i], i));
        }

        xEntry.add("House");
        xEntry.add("Groceries");
        xEntry.add("Gas");
        xEntry.add("Electric");
        xEntry.add("Entertainment");
        xEntry.add("Cable");
        xEntry.add("Car");
        xEntry.add("Clothes");
        xEntry.add("Dr");
        xEntry.add("House Items");



        PieDataSet pieDataSet = new PieDataSet(yEntry, String.valueOf(xEntry));
        pieDataSet.setSliceSpace(5);
        pieDataSet.setValueTextSize(18);

        ArrayList<Integer>colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.RED);
        colors.add(Color.CYAN);
        colors.add(Color.DKGRAY);
        colors.add(Color.GRAY);
        colors.add(Color.MAGENTA);
        colors.add(Color.GREEN);
        colors.add(Color.YELLOW);
        colors.add(Color.WHITE);
        colors.add(Color.GREEN);
        colors.add(Color.RED);
        colors.add(Color.CYAN);


        pieDataSet.setColors(colors);
        PieData pieData = new PieData(pieDataSet);

        piechart.setData(pieData);

        piechart.invalidate();

piechart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Object Example = e.getData();
        Toast.makeText(PieResults.this,"Bill: "+ xEntry.get((Integer)Example), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected() {

    }
});



        //region UNUSED


     //   setContentView(R.layout.activity_pie_results);
    }

//    private void addDataSet() {
//       ArrayList<PieEntry>yEntry = new ArrayList<>();
//        ArrayList<String>xEntry = new ArrayList<>();
//        for(int i = 0; i < yData.length; i++){
//            yEntry.add(new PieEntry(yData[i], i));
//        }
//        for(int i = 0; i < xData.length; i++){
//            xEntry.add(xData[i]);
//        }
//PieDataSet pieDataSet = new PieDataSet(yEntry, "Bills");
//pieDataSet.setSliceSpace(2);
//        pieDataSet.setValueTextSize(12);
//
//        ArrayList<Integer>colors = new ArrayList<>();
//        colors.add(Color.BLUE);
//        colors.add(Color.RED);
//        colors.add(Color.CYAN);
//        colors.add(Color.DKGRAY);
//        colors.add(Color.GRAY);
//        colors.add(Color.MAGENTA);
//        colors.add(Color.GREEN);
//        colors.add(Color.YELLOW);
//        colors.add(Color.WHITE);
//        colors.add(Color.GREEN);
//        colors.add(Color.RED);
//        colors.add(Color.CYAN);
//
//        pieDataSet.setColors(colors);
//        PieData pieData = new PieData(pieDataSet);
//        pieChart.setData(pieData);
//        pieChart.invalidate();
//
//
//
//    }

}
