package com.example.mathewholden.mholden_final;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import static com.example.mathewholden.mholden_final.R.id.resultsList;

public class ResultsActivity extends AppCompatActivity {

    private Button btnBack;
    DatabaseHandler db;
    ListView results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = new DatabaseHandler(this);

        btnBack = (Button) findViewById(R.id.btnBack);
        results = (ListView) findViewById(resultsList);


        Bundle extras = getIntent().getExtras();
        Budget newBudget = extras.getParcelable("yourKey");

        db.addOrder(newBudget);

        List<Budget> budgetList = db.getAllBudgets();


        //simple_list_item_1 contains only a TextView
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);

        // adding entries in List
        for (int counter = 0; counter < budgetList.size(); counter++) {
            adapter.add("ID: "+budgetList.get(counter).getId()+
                    " Start Budget: "+budgetList.get(counter).getBudget()+
                    "Bill: "+ budgetList.get(counter).getBill()+
                    "Remaining "+ budgetList.get(counter).getRemain());
        }

        // setting adapter to list
        results.setAdapter(adapter);





        btnBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


//    @Override
//    public void finish() {
//        Intent i = new Intent();
//        String numberOfOrders = "Number of Orders = " + db.getOrdersCount();
//        i.putExtra("returnkey", numberOfOrders);
//        setResult(RESULT_OK, i);
//        // Close
//        super.finish();
//    }
//

}

